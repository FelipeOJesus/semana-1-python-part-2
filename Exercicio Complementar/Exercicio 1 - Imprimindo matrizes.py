def imprime_matriz(matriz):
    for i in range(len(matriz)):
        for j in range(len(matriz[0])):
            if(matriz[i][j] == matriz[i][len(matriz[0])-1]):
               print(matriz[i][j])
            else:
               print(matriz[i][j], end=" ")


def main():
    minha_matriz = [[1], [2], [3]]
    imprime_matriz(minha_matriz)

    minha_matriz = [[1, 2, 3], [4, 5, 6]]
    imprime_matriz(minha_matriz)


main()
               
