def soma_matrizes(m1, m2):
    somaMatriz = []
    if(len(m1) == len(m2) and len(m1[0]) == len(m2[0])):
        for i in range(len(m1)):
            linha = []
            for j in range(len(m2[0])):
                linha.append(m1[i][j] + m2[i][j])
            somaMatriz.append(linha)
        return somaMatriz
    return False

def main():
    m1 = [[1, 2, 3], [4, 5, 6]]
    m2 = [[2, 3, 4], [5, 6, 7]]
    print(soma_matrizes(m1, m2))

    m1 = [[1], [2], [3]]
    m2 = [[2, 3, 4], [5, 6, 7]]
    print(soma_matrizes(m1, m2))

main()

